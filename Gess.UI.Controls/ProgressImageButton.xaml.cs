﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using Color = System.Windows.Media.Color;

namespace Gess.UI.Controls
{
    /// <summary>
    /// Interaction logic for CheckImage.xaml
    /// </summary>
    public partial class ProgressImageButton : UserControl
    {
        public ProgressImageButton()
        {
            InitializeComponent();

            ImageHeight = DefaultImageHeight;

            ImageWidth = DefaultImageWidth;
        }

        #region Private members

        private Orientation _orientation;

        private ImageTextOrder _order;

        const int DefaultImageWidth = 32;

        const int DefaultImageHeight = 32;

        #endregion
        public ImageSource ActiveImage { get; set; }

        public ImageSource CompletedImage { get; set; }

        public ImageSource UncompletedImage { get; set; }

        public double ImageWidth
        {
            get
            {
                return StatusImage.Width;
            }
            set
            {
                StatusImage.Width = value;
            }
        }

        public double ImageHeight
        {
            get
            {
                return StatusImage.Height;
            }
            set
            {
                StatusImage.Height = value;
            }
        }

        public Brush ActiveForeground { get; set; }

        public Brush CompletedForeground { get; set; }

        public Brush UncompletedForeground { get; set; }

        public ImageTextOrder Order 
        {
            get 
            { 
                return this._order; // return (ImageTextOrder) this.GetValue(OrderProperty);
            }
            set
            {
                // this.SetValue(OrderProperty, value);
                this._order = value;
                Reposition();
            }
        }

        #region Design-time dependency properties

        //public static readonly DependencyProperty OrientationProperty = DependencyProperty.Register("Orientation", typeof(Orientation), typeof(ProgressImageButton));

        //public static readonly DependencyProperty OrderProperty = DependencyProperty.Register("Order", typeof(ImageTextOrder), typeof(ProgressImageButton));

        public static readonly DependencyProperty ImageMarginProperty = DependencyProperty.Register("ImageMargin", typeof(Thickness), typeof(ProgressImageButton));

        public static readonly DependencyProperty TextMarginProperty = DependencyProperty.Register("TextMargin", typeof(Thickness), typeof(ProgressImageButton));

        #endregion

        public Orientation Orientation
        {
            get
            {
                return _orientation; // (Orientation) this.GetValue(OrientationProperty);
            }
            set
            {
                _orientation = value; // this.SetValue(OrientationProperty, value);
                Reposition();
            }
        }

        public string Text 
        {
            get
            {
                return LabelText.Text;
            }
            set
            {
                LabelText.Text = value;
            }
        }

        protected void Reposition()
        {
            this.WrapPanel.Orientation = this.Orientation;

            if (this.Order == ImageTextOrder.TextFirst)
            {
                this.WrapPanel.Children.Clear();
                this.WrapPanel.Children.Add(LabelText);
                this.WrapPanel.Children.Add(StatusImage);
            }
            else
            {
                this.WrapPanel.Children.Clear();
                this.WrapPanel.Children.Add(StatusImage);
                this.WrapPanel.Children.Add(LabelText);
            }
        
            if (this.Orientation == Orientation.Horizontal)
            {
                StatusImage.VerticalAlignment = VerticalAlignment.Top;
                LabelText.VerticalAlignment = VerticalAlignment.Top;

                if (this.Order == ImageTextOrder.ImageFirst)
                {
                    StatusImage.HorizontalAlignment = HorizontalAlignment.Left;
                    LabelText.HorizontalAlignment = HorizontalAlignment.Right;
                }
                else
                {
                    StatusImage.HorizontalAlignment = HorizontalAlignment.Right;
                    LabelText.HorizontalAlignment = HorizontalAlignment.Left;
                }
            }
            else
            {
                StatusImage.HorizontalAlignment = HorizontalAlignment.Center;
                LabelText.HorizontalAlignment = HorizontalAlignment.Center;

                if (this.Order == ImageTextOrder.TextFirst)
                {
                    StatusImage.VerticalAlignment = VerticalAlignment.Top;
                    LabelText.VerticalAlignment = VerticalAlignment.Bottom;
                }
                else
                {
                    StatusImage.VerticalAlignment = VerticalAlignment.Bottom;
                    LabelText.VerticalAlignment = VerticalAlignment.Top;
                }
            }
        }

        public Thickness ImageMargin
        {
            get
            {
                return (Thickness) this.GetValue(ImageMarginProperty);
            }
            set
            {
                StatusImage.Margin = value;
                this.SetValue(ImageMarginProperty, value);
            }
        }

        public Thickness TextMargin
        {
            get
            {
                return (Thickness) this.GetValue(TextMarginProperty);
            }
            set
            {
                LabelText.Margin = value;
                this.SetValue(TextMarginProperty, value);
            }
        }

        protected void Redraw()
        {
            try
            {
                string imageUri;
                ImageSource statusImageSrc;

                switch (_progressState)
                {
                    case ProgressState.Active:

                        statusImageSrc = (BitmapImage) this.Resources["ActiveImage"];
                        LabelText.Style = (Style) this.Resources["ActiveStyle"];

                        break;

                    case ProgressState.Completed:

                        statusImageSrc = (BitmapImage) this.Resources["CompletedImage"];
                        LabelText.Style = (Style) this.Resources["CompletedStyle"];
                        break;

                    case ProgressState.Uncompleted:

                        statusImageSrc = (BitmapImage) this.Resources["UncompletedImage"];
                        LabelText.Style = (Style) this.Resources["UncompletedStyle"];
                        break;

                    default:
                        return;
                }

                StatusImage.Source = statusImageSrc;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + ex.StackTrace);
            }
            finally
            {
                this.InvalidateVisual();
            }
        }

        private ProgressState _progressState;

        public ProgressState ProgressState 
        {
            get
            {
                return _progressState;
                // return (ProgressState) this.GetValue(ProgressStateProperty);
            }
            set
            {
                _progressState = value;
                // this.SetValue(ProgressStateProperty, value);
                Redraw();
            }
        }

        protected virtual void OnClick()
        {
            if (this.ProgressState != ProgressState.Uncompleted)
                this.ProgressState = ProgressState.Active;
        }

        public event EventHandler Click;

        protected void RaiseClick()
        {
            var handlers = this.Click;

            if (handlers != null)
                handlers(this, new EventArgs());
        }
    }

    public enum ProgressState
    {
        Completed,

        Active,

        Uncompleted
    }

    public enum ImageTextOrder
    {
        TextFirst,

        ImageFirst,
    }
}
