﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gess.UI.Controls
{
    /// <summary>
    /// Interaction logic for DataBlock.xaml
    /// </summary>
    public partial class DataBlock : UserControl
    {
        public DataBlock()
        {
            InitializeComponent();
        }

        public static DependencyProperty HeaderTextProperty = DependencyProperty.Register("HeaderText", typeof(string), typeof(DataBlock));

        public string HeaderText
        {
            get
            {
                return (string) this.GetValue(HeaderTextProperty);
            }
            set
            {
                this.SetValue(HeaderTextProperty, value);
            }
        }

        public bool ShowLine { get; set; }

        public Brush LineColor { get; set; }

        public double LineHeight { get; set; }

        public double LineWidth { get; set; }

        public Thickness BoxMargin { get; set; }

        public Thickness BoxPadding { get; set; }

        public List<string> Labels { get; protected set; }

        public List<string> Values { get; protected set; }

        public List<object> DataBOund { get; protected set; }

        protected virtual void Redraw()
        {
            
        }

        public double LabelFontSize { get; set; }

        public double ValueFontSize { get; set; }

        public bool trimLabel { get; set; }
    }
}
